<?php
include('lib/sluggy.php');
if(isset($_POST['slugmit'])){
		$input = $_POST['input'];
		$output = new Sluggy($input);
		$slugs = $output->sluggy($input);
		$form = buildForm($input, $slugs);
	}elseif(isset($_POST['reset'])){
		header('Location: index.php');
	}else{
		$form = buildForm();
	}

function buildForm($input = '', $slugs = ''){
		//$copyBtn = '<button class="btn btn-primary btn-lg" id="copy"  data-clipboard-target="output" title="Click to copy">Copy</button>';
		$copyBtn = '<p id="copy">Use CTRL + C to copy to clipboard</p>';
		$slugBtn = '<input class="btn btn-primary btn-lg" id="slugmit" name="slugmit" type="submit" value="Get Slugs &raquo;"> ';
		$resetBtn = '<input class="btn btn-primary btn-lg" id="reset" name="reset" type="submit" value="Reset">';
	$form = '<form id="sluggy" name="sluggy" action="#" method="post" accept-charset="UTF-8">
			<div class="field">
					<textarea id="input" name="input" value="" placeholder="Enter items to convert, separated by commas" required wrap="soft">'.$input.'</textarea>
				</div>
				<div class="field">
					<textarea id="output" name="output" data-clipboard-target="clipboard_textarea" value="" placeholder="Your wordpress-friendly slugs will appear here">'.$slugs.'</textarea>
				</div>	
			<div class="field">';
			if(isset($_POST['slugmit'])){
				$form .= $copyBtn;
				$form .= $resetBtn;
			}else{
				$form .= $slugBtn;			
			}
			$form .= '</div></form>';
	
	return $form;
}

?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Slug.Space</title>
        <meta name="description" content="Create wordpress friendly slugs from any text string">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/main.css">

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">Slug.Space</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Slugs!</h1>
        <p>Create wordpress-friendly slugs.  Enter text in the box below and click "Get Slugs".</p>
		<?php      
		echo $form;
		?>	  
	  </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <!--div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div-->
      </div>

      <footer>
        <p>&copy; Slug.Space <?php echo date('Y');?></p>
      </footer>
    </div> <!-- /container -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/plugins.js"></script>
		
		<script src="assets/js/ZeroClipboard.js"></script>
		<script src="assets/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-62357238-1','auto');ga('send','pageview');
        </script>
    </body>
</html>
