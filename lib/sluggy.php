<?php

Class Sluggy{

	public $input;
	private $output;
	private $slugs;
	
	public function __construct($input){
		$this->input = $input;
		return $this->input;
	}
	
	private function stripSpecials($input){
		$output = preg_replace('/[^A-Za-z0-9\-]/', '', $input);
		return $output;
	}
	
	private function stripCaps($input){
		$output = strtolower($input);
		return $output;	
	}
	
	private function addDashes($input){
		$output = str_replace(' ', '-', $input); // Replaces all spaces with hyphens.
		return $output;
	}
	
	private function stripDoubleDashes($input){
		$output = str_replace(' ', '--', $input); // Replaces all double spaces with hyphens.
		return $output;
	}
	
	private function stripLastSlash($input){
		if (substr($input,-1)=="-"){
			$input = substr($input,0,-1);
		}
		return $input;
	}
	
	private function createOutput($input){
		$slugs = '';
		foreach($input as $k=>$v){
			if($v!=''){
				$slugs .= $v.'&#13;&#10;';
			}
		}
		
		return $slugs;
	}
	
	private function createArray($input){
		$input = str_replace(', ', ',', $input);//handle whitespace after commas
		$output = explode(',', $input);//get rid of commas, create array
		return $output;
	}
	
	public function sluggy($input){
		$array = array();
		
		$input = $this->createArray($input);
		foreach($input as $k=>$v){
			$input = $this->addDashes($v);
			$input = $this->stripCaps($input);
			$input = $this->stripSpecials($input);
			$input = $this->stripDoubleDashes($input);
			$input = $this->stripLastSlash($input);
			$output = array_push($array, $input);
		}

		$html = $this->createOutput($array);
		
		return $html;
	}
	
}
